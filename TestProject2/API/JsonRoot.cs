﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Vidjet
{
    public partial class JsonRoot
    {
        [JsonPropertyName("code")]
        public string Code { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("details")]
        public Details Details { get; set; }
    }

    public partial class Details
    {
        [JsonPropertyName("access_token")]
        public string AccessToken { get; set; }

        [JsonPropertyName("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("expire_date")]
        public DateTimeOffset ExpireDate { get; set; }

        [JsonPropertyName("transactionId")]
        public long TransactionId { get; set; }

        [JsonPropertyName("dateTimeUtc")]
        public DateTimeOffset DateTimeUtc { get; set; }

        [JsonPropertyName("dateCreatedUtc")]
        public DateTimeOffset DateCreatedUtc { get; set; }

        [JsonPropertyName("sum")]
        public long Sum { get; set; }

        [JsonPropertyName("compensationSum")]
        public object CompensationSum { get; set; }

        [JsonPropertyName("surchargeSum")]
        public object SurchargeSum { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
    
}
