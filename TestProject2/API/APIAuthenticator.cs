﻿using RestSharp.Authenticators;
using RestSharp;
using System;

namespace Vidjet
{
    public class APIAuthenticator : AuthenticatorBase
    {
        readonly string _username;
        public APIAuthenticator(string username) : base("")
        {
            _username = username;
        }

        protected override async ValueTask<Parameter> GetAuthenticationParameter(string accessToken)
        {
            Token = string.IsNullOrEmpty(Token) ? await GetToken() : Token;
            return await new ValueTask<Parameter>(new HeaderParameter(KnownHeaders.Authorization, Token));
        }

        private async Task<string> GetToken()
        {
            var options = new RestClientOptions("https://api-test.nalunch.me/v3/");
            using (var client = new RestClient(options))
            {
                var creds = "{\"username\": \""+_username+"\",\r\n  \"password\": \"1234\"\r\n}";
                var request = new RestRequest("account/auth");
                request.AddStringBody(creds, ContentType.Json);
                var response = await client.PostAsync<JsonRoot>(request);
                Console.WriteLine(response.ToString());
                return $"Bearer {response.Details.AccessToken}";
            }
        }
    }
}
