﻿using OpenQA.Selenium;

namespace VidjetSpace
{
     class Helper:HelperBase
    {
        private AuthPage auth;
        private DashboardPage dashboard;
        private Navigation navigation;
        private DBqueries db;

        public Helper(IWebDriver driver)
        {
            this.driver = driver;
            this.auth = new AuthPage(driver);
            this.navigation = new Navigation(driver);
            this.dashboard = new DashboardPage(driver);
            this.db = new DBqueries();
        }

        public DashboardPage Dashboard
        {
            get
            {
                return dashboard;
            }
        }
        public AuthPage Auth
        {
            get
            {
                return auth;
            }
        }
       
        public Navigation Navigation
        {
            get
            {
                return navigation;
            }
        }

        public DBqueries DB
        {
            get
            {
                return db;
            }
        }

    }
}
