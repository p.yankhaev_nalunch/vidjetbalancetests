﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Data.SqlClient;

namespace VidjetSpace
{
    internal class TestBase:HelperBase
    {
        [SetUp]
        public void SetupTest()
        {
            Helper helper = new Helper(driver);
            var options = new ChromeOptions();
            driver = new ChromeDriver(options);
            driver.Manage().Window.Maximize();
            helper.DB.DeleteWeekend();
        }

        [TearDown]
        public void TeardownTest()
        {
            Helper helper = new Helper(driver);
            helper.DB.DeleteWeekend();
            driver.Close();
            driver.Quit();
        }
    }
}
