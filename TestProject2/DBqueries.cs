﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidjetSpace
{
    internal class DBqueries
    {
        #region SetBalance
        public void SetBalance(int acc, int com, int max, int lim, bool iscomacc, string userName)
        {
            SetAccumulatedCompensation(acc, userName);
            SetCompensation(com, userName);
            SetMaxBalance(max, userName);
            UpdateLimit(lim, userName);
            IsCompensationAccumulated(iscomacc, userName);
        }

        public void IsCompensationAccumulated(bool iscomacc, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set IsCompensationAccumulated=@iscomacc WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.Add(new SqlParameter("@iscomacc", SqlDbType.Int) { Value = iscomacc });
                    command.Parameters.Add(new SqlParameter("@user", SqlDbType.NVarChar, 50) { Value = user });
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetAccumulatedCompensation(int acccomp, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set AccumulatedCompensationSum=@acccomp WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.Add(new SqlParameter("@acccomp", SqlDbType.Int) { Value = acccomp });
                    command.Parameters.Add(new SqlParameter("@user", SqlDbType.NVarChar, 50) { Value = user });
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetCompensation(int comp, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set Compensation=@comp WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@comp", comp);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetMaxBalance(int max, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set MaxBalance=@max WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@max", max);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetTransactionSum(int trans, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Transaction] SET sum=@trans, CompensationSum=@trans, PurseSum=@trans, SurchargeSum=@trans WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@trans", trans);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateLimit(int limit, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set FizikLimit=@lim WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@lim", limit);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateIsFizikBlocked(int block, string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "update [naLunch_Test].[dbo].[Staff] set IsFizikBlocked=@block where Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@block", block);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }
        public string OrderStatus()
        {
            string queryString = "SELECT TOP(1) Message FROM[naLunch_Test].[dbo].[OrderStaffStatusHistory] WHERE OrderStaffId=(SELECT TOP(1) Id FROM [naLunchCastles].[dbo].[OrderStaff] WHERE StaffId=240682 ORDER BY CreatedUtc DESC) ORDER BY HistoryDateUtc DESC";
            
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                var status = (string)command.ExecuteScalar();
                return status;
            }
        }
        #endregion

        #region BindCard

        public void UpdateBindCard(string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                        command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET IsFizik=1, IsNewFizikLimit=1 WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                        command.Parameters.AddWithValue("@user", user);
                        command.ExecuteNonQuery();                            
                }
            }
        }

        public int IsCardBinded(string user)
        {
            string queryString = "SELECT COUNT(Id) FROM [naLunch_Test].[dbo].[BindingCard] WHERE IsDeleted=0 AND staffId=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)\r\n";
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@user", user);
                var status = (int)command.ExecuteScalar();
                return status; 
            }
        }
        

        public void InsertCard(string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO [naLunch_Test].[dbo].[BindingCard] (PAN, Expiration, AlfaOrderId, StaffId, CreatedUtc, IsDeleted, BindingId)\r\n  VALUES (4111111111111111,'202412', 10372, (SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user), GETDATE(), 0, NEWID())";
                        command.Parameters.AddWithValue("@user", user);
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion

        #region CountingCompensationType
        public void CompensationCountingType(string user, int type)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET CompensationType=@type WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@type", type);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CompensationDaily(string user, int comp)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET DailyCompensation=@comp WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@comp", comp);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CalculateForWeekends(string user, bool calc)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET CalculateForWeekends=@calc WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@calc", calc);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion

        #region Weekend
        public int IsWeekend()
        {
            string queryString = "SELECT Count(Date) FROM [naLunch_Test].[dbo].[DayOffCalendar] WHERE Date=CONVERT(DATE, GETDATE())";
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                var status = (int)command.ExecuteScalar();
                return status;
            }
        }

        public void InsertWeekend()
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO [naLunch_Test].[dbo].[DayOffCalendar] (Date) VALUES (GETDATE())";
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public void DeleteWeekend()
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM [naLunch_Test].[dbo].[DayOffCalendar] WHERE Date=CONVERT(DATE, GETDATE())";
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion
        public void SeenOnboarding(string user)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[User] SET HasSeenOnBoarding=1, IsNewUser=0 WHERE Name=@user";
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CompensationRule(string user, int rule)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET CompensationRuleId=@rule WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@rule", rule);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void CompensationOnWeekend(string user, bool weekend)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET CompensationOnWeekends=@weekend WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@weekend", weekend);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void ChargeOnWeekend(string user, bool weekend)
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff] SET СhargeOnWeekends=@weekend WHERE Id=(SELECT StaffId FROM [naLunch_Test].[dbo].[User] WHERE Name=@user)";
                    command.Parameters.AddWithValue("@weekend", weekend);
                    command.Parameters.AddWithValue("@user", user);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void ExtendVacation()
        {
            using (SqlConnection connection = new SqlConnection("Data Source = 31.129.57.14; Initial Catalog = naLunch_Test; User ID=SRV_NLN_DBUser_Local; Password=9kt07uoNqq"))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "UPDATE [naLunch_Test].[dbo].[Staff_FutureSetting] SET StartDateTimeUtc=DATEADD(MONTH, 6, GETDATE()) WHERE Id IN (97,99,101,103,105,107,109)";
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}
