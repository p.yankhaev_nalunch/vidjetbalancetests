﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using RestSharp;
using RestSharp.Authenticators;
using Vidjet;

namespace VidjetSpace
{
    //список тестовых пользователей тут https://docs.google.com/spreadsheets/d/1wYkrfGlTt4MUUwfoLO7PALrKjuA0zQVRLUEWM4kHAzQ/edit?usp=sharing
    //список тест-кейсов тут https://app.qase.io/run/STAFF/dashboard/363
    [TestFixture]
    class Vidjet : TestBase
    {
        int _accumulatedCompensationSum;
        int _compensationSum;
        int _maxBalanceSum;
        int _fizikLimitSum;
        int _compensationType;
        int _compensationRule;
        int _dailyCompensation;
        int _transactionSum;
        bool _isCompensationAccumulated;
        string _userName;
        string _vacationNoAccComp = "В это время компенсация от работодателя не начисляется. В случае, если даты отпуска не верны, пожалуйста, обратитесь к менеджеру своей компании, который отвечает за компенсацию.";
        string _vacationAccComp = "В это время компенсация от работодателя не начисляется, но вы можете воспользоваться вашей накопленной компенсацией. В случае, если даты отпуска не верны, пожалуйста, обратитесь к менеджеру своей компании, который отвечает за компенсацию.";
        string _apiBaseV3 = "https://api-test.nalunch.me/v3/";


        #region IsVidgetPresent   
        [Test, Description("Виджет не должен отображаться если у сотрудника нет карты, макс баланса и компенсации")]
        public void VidjetIsNotAvailable()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test1";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            Assert.That(helper.Dashboard.IsVidjetAvailable(), Is.EqualTo(false));
        }

        [Test, Description("Виджет отображается если у сотрудника есть банковская карта")]
        public void VidjetAvailable_bankCard()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test2";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == null)
              {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
              }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName); 
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.IsVidjetAvailable(), Is.EqualTo(true));
        }

        [Test, Description("Виджет отображается если у сотрудника есть компенсация")]
        public void VidjetAvailable_compensation()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 3000;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test3";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.IsVidjetAvailable(), Is.EqualTo(true));
        }

        [Test, Description("Виджет отображается если у сотрудника есть накопленная компенсация")]
        public void VidjetAvailable_accumulatedCompensation()
        {
            _accumulatedCompensationSum = 500;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test4";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.IsVidjetAvailable(), Is.EqualTo(true));
        }

        [Test, Description("Виджет отображается если у сотрудника есть макс баланс")]
        public void VidjetAvailable_maxBalance()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test5";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.IsVidjetAvailable(), Is.EqualTo(true));
        }
        #endregion

        #region RegularVidjetView

        [Test, Description("Отображается общая сумма с овердрафтом")]
        public void VidjetView_compensation_overdraft()
        {
            _accumulatedCompensationSum = 200;
            _compensationSum = 100;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test6";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.WholeSumWithOverdraft(), Is.EqualTo("Общая сумма с овердрафтом " + _maxBalanceSum + " ₽"));
        }

        [Test, Description("Макс баланс не отображается с привязаной БК")]
        public void VidjetView_compensation_withcBankCard_withoutOverdraft()
        {
            _accumulatedCompensationSum = 200;
            _compensationSum = 100;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test7";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == null)
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Макс баланс не отображается с накопительной компенсацией")]
        public void VidjetView_IsCompensationAccumulated_True_WithoutOverdraft()
        {
            _accumulatedCompensationSum = 200;
            _compensationSum = 100;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test8";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
        }

        [Test, Description("Личные средства")]
        public void VidjetView_FizikLimit()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 100;
            _isCompensationAccumulated = true;
            _userName = "vidj_test9";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Личные средства заблокированы")]
        public void VidjetView_FizikBlocked()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 1000;
            _isCompensationAccumulated = true;
            _userName = "vidj_test10";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Сумма компенсации")]
        public void VidjetView_Compensation()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 300;
            _maxBalanceSum = 300;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test11";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum));
        }

        [Test, Description("Сумма компенсации из накопленной компенсации")]
        public void VidjetView_CompensationAccumulated()
        {
            _accumulatedCompensationSum = 300;
            _compensationSum = 0;
            _maxBalanceSum = 300;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test12";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum));
        }

        [Test, Description("Личные средства + накопленная компенсация")]
        public void VidjetView_CompensationAccumulated_FizikLimit()
        {
            _accumulatedCompensationSum = 300;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 100;
            _isCompensationAccumulated = true;
            _userName = "vidj_test13";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum + _fizikLimitSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Компенсация и заблокированный лимит")]
        public void VidjetView_Compensation_FizikBlocked()
        {
            _accumulatedCompensationSum = 200;
            _compensationSum = 100;
            _maxBalanceSum = 500;
            _fizikLimitSum = 500;
            _isCompensationAccumulated = true;
            _userName = "vidj_test14";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFizikBlocked2(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Макс баланс не отображается если меньше суммы компенсации и накопленной компенсации")]
        public void VidjetView_MaxBalanceLessThenActualCompensation()
        {
            _accumulatedCompensationSum = 1000;
            _compensationSum = 500;
            _maxBalanceSum = 600;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test15";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
        }

        [Test, Description("Виджет овердрафт")]
        public void VidjetView_Overdraft()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 600;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test16";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Овердрафт"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_maxBalanceSum));
        }

        #endregion

        #region VacationVidjetView

        [Test, Description("Личный лимит заблокирован в отпуске")]
        public void VacationVidjetView_FizikBlocked()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 3000;
            _isCompensationAccumulated = false;
            _userName = "vidj_test17";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationLimitSumBlocked(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VacationFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Личный лимит заблокирован в отпуске + накопленная компенсация")]
        public void VacationVidjetView_FizikBlocked_AccumulatedCompensation()
        {
            _accumulatedCompensationSum = 500;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 3000;
            _isCompensationAccumulated = true;
            _userName = "vidj_test18";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Накопленная компенсация"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum));
            Assert.That(helper.Dashboard.VacationFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationLimitSumBlocked(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VacationFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Виджет в отпуске с личным лимитом + накопленная компенсация")]
        public void VacationVidjetView_FizikLimit_AccumulatedCompensation()
        {
            _accumulatedCompensationSum = 500;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 3000;
            _isCompensationAccumulated = true;
            _userName = "vidj_test19";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Накопленная компенсация"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum));
            Assert.That(helper.Dashboard.VacationFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет в отпуске с личным лимитомя")]
        public void VacationVidjetView_FizikLimit()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 3000;
            _isCompensationAccumulated = true;
            _userName = "vidj_test20";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет в отпуске с накопительной компенсацией, но без накопленной")]
        public void VacationVidjetView_isCompensationAccumulated_True()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test21";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
        }

        [Test, Description("Виджет в отпуске с накопленной компенсацией")]
        public void VacationVidjetView_AccumulatedCompensation()
        {
            _accumulatedCompensationSum = 1000;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test22";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Накопленная компенсация"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum));
        }

        #endregion

        #region CountCompensationTypeVidjet
        [Test, Description("Виджет с вычисляемой компенсацией")]
        public void VidjetView_CompensationCount()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _dailyCompensation = 100;
            _compensationRule = 1;
            _compensationType = 1;
            _userName = "vidj_test23";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.CompensationDaily(_userName, 100);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationCountingType(_userName, _compensationType);
            helper.DB.CompensationRule(_userName, _compensationRule);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum));
        }

        [Test, Description("Виджет с вычисляемой компенсацией и личным лимитом")]
        public void VidjetView_CompensationCount_FizikLimit()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 500;
            _isCompensationAccumulated = false;
            _dailyCompensation = 100;
            _compensationRule = 1;
            _compensationType = 1;
            _userName = "vidj_test24";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.CompensationDaily(_userName, 100);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationCountingType(_userName, _compensationType);
            helper.DB.CompensationRule(_userName, _compensationRule);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum+_fizikLimitSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет с вычисляемой компенсацией и накопительной компенсацией")]
        public void VidjetView_CompensationCount_IsCompensationAccumulated_True()
        {
            _accumulatedCompensationSum = 300;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _dailyCompensation = 100;
            _compensationRule = 1;
            _compensationType = 1;
            _userName = "vidj_test25";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.CompensationDaily(_userName, 100);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationCountingType(_userName, _compensationType);
            helper.DB.CompensationRule(_userName, _compensationRule);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum+ _accumulatedCompensationSum));
        }

        [Test, Description("Виджет с вычисляемой компенсацией, накопительной компенсацией и личным лимитом")]
        public void VidjetView_CompensationCount_FizikLimit_IsCompensationAccumulated_True()
        {
            _accumulatedCompensationSum = 300;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 200;
            _isCompensationAccumulated = true;
            _dailyCompensation = 100;
            _compensationRule = 1;
            _compensationType = 1;
            _userName = "vidj_test26";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.CompensationDaily(_userName, 100);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationCountingType(_userName, _compensationType);
            helper.DB.CompensationRule(_userName, _compensationRule);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum + _fizikLimitSum + _accumulatedCompensationSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_compensationSum + _accumulatedCompensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет с вычисляемой компенсацией, накопительной компенсацией и личным лимитом по выходным с начислением по выходным и включая выходные")]
        public void VidjetView_CompensationCount_FizikLimit_IsCompensationAccumulated_True_CompensationOnWeekend_CalculateOnWeekend()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _dailyCompensation = 100;
            _compensationRule = 1;
            _compensationType = 1;
            _userName = "vidj_test27";
            Helper helper = new Helper(driver);
            if (helper.DB.IsWeekend()==0) 
            {
                helper.DB.InsertWeekend();
            }
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.CompensationDaily(_userName, 100);
            helper.DB.CalculateForWeekends(_userName, true);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationCountingType(_userName, _compensationType);
            helper.DB.CompensationRule(_userName, _compensationRule);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum));
        }
        #endregion

        #region VidjetOnWeekends
        [Test, Description("Виджет виджета по выходным с лимитом и компенацией без начисления по выходным")]
        public void VidjetViewOnWeekend_FizikLimit_NoCompensationOnWeekend()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 100;
            _maxBalanceSum = 100;
            _fizikLimitSum = 200;
            _isCompensationAccumulated = true;
            _userName = "vidj_test28";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
           /* if (helper.DB.IsCardBinded(_userName)==0);
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/            
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            if (helper.DB.IsWeekend() == 0)
            {
                helper.DB.InsertWeekend();
            }
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, false);
            helper.DB.CompensationOnWeekend(_userName, false);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_fizikLimitSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет виджета по выходным с компенсацией с начислением по выходным")]
        public void VidjetViewOnWeekend_CompensationOnWeekend()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 100;
            _maxBalanceSum = 100;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = true;
            _userName = "vidj_test29";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            if (helper.DB.IsWeekend() == 0)
            {
                helper.DB.InsertWeekend();
            }
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum));
        }

        [Test, Description("Виджет виджета по выходным с компенсацией и макс балансом без начисления по выходным")]
        public void VidjetViewOnWeekend_Overdraft_NoCompensationOnWeekend()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 100;
            _maxBalanceSum = 200;
            _fizikLimitSum = 0;
            _isCompensationAccumulated = false;
            _userName = "vidj_test30";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            if (helper.DB.IsWeekend() == 0)
            {
                helper.DB.InsertWeekend();
            }
            helper.DB.SeenOnboarding(_userName);
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, false);
            helper.DB.CompensationOnWeekend(_userName, false);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(0));
        }
        #endregion

        #region VidjetWithTransactions
        [Test, Description("Отображается общая сумма с овердрафтом после транзакции")]
        public void VidjetView_Transaction_Compensation_Overdraft()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 300;
            _maxBalanceSum = 500;
            _fizikLimitSum = 0;
            _transactionSum = 400;
            _isCompensationAccumulated = false;
            _userName = "vidj_test31";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.WholeSumWithOverdraft(), Is.EqualTo("Общая сумма с овердрафтом " + (_maxBalanceSum - _transactionSum)+ " ₽"));
        }

        [Test, Description("Отображается сумма овердрафта после транзакции")]
        public void VidjetView_Transaction_Overdraft()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 2500;
            _fizikLimitSum = 0;
            _transactionSum = 500;
            _isCompensationAccumulated = false;
            _userName = "vidj_test32";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Овердрафт"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_maxBalanceSum - _transactionSum));
        }

        [Test, Description("Отображается общая сумма с компенсацией и лимитом после транзакции")]
        public void VidjetView_Transaction_Compensation_FizikLimit()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 300;
            _maxBalanceSum = 300;
            _fizikLimitSum = 2000;
            _transactionSum = 500;
            _isCompensationAccumulated = false;
            _userName = "vidj_test43";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
           /* if (helper.DB.IsCardBinded(_userName) == 0) ;
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_fizikLimitSum+_compensationSum - _transactionSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(1800));
        }

        [Test, Description("Отображается общая сумма с компенсацией и заблокированным лимитом после транзакции")]
        public void VidjetView_Transaction_Compensation_FizikBlocked()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 500;
            _maxBalanceSum = 500;
            _fizikLimitSum = 2000;
            _transactionSum = 800;
            _isCompensationAccumulated = false;
            _userName = "vidj_test34";
            Helper helper = new Helper(driver);
            helper.DB.UpdateIsFizikBlocked(0, _userName);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0);
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            Thread.Sleep(3000);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFizikBlocked2(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Отображается сумма овердрафта")]
        public void VidjetView_Transaction_Overdraft_IsFizik()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 2500;
            _fizikLimitSum = 0;
            _transactionSum = 500;
            _isCompensationAccumulated = false;
            _userName = "vidj_test35";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0);
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Овердрафт"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_maxBalanceSum));
        }

        [Test, Description("Отображается заблокированный лимит после транзакции")]
        public void VidjetView_Transaction_FizikBlocked()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 800;
            _transactionSum = 500;
            _isCompensationAccumulated = false;
            _userName = "vidj_test36";
            Helper helper = new Helper(driver);
            helper.DB.UpdateIsFizikBlocked(0, _userName);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0);
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Тип компенсации - неделя, компенсация + накопленная компенсация + личный лимит")]
        public void VidjetView_Transaction_Compensation_AccumulatedCompensation_FizikLimit_Week()
        {
            _accumulatedCompensationSum = 600;
            _compensationSum = 300;
            _maxBalanceSum = 300;
            _fizikLimitSum = 100;
            _transactionSum = 950;
            _isCompensationAccumulated = true;
            _userName = "vidj_test47";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0) ;
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationRule(_userName,1);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(50));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(50));
        }

        [Test, Description("Тип компенсации - месяц, компенсация + накопленная компенсация + личный лимит")]
        public void VidjetView_Transaction_Compensation_AccumulatedCompensation_FizikLimit_Month()
        {
            _accumulatedCompensationSum = 600;
            _compensationSum = 300;
            _maxBalanceSum = 300;
            _fizikLimitSum = 100;
            _transactionSum = 950;
            _isCompensationAccumulated = false;
            _userName = "vidj_test48";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0) ;
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.CompensationRule(_userName, 1);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(50));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(50));
        }

        [Test, Description("Виджет в отпуске с личным лимитом + накопленная компенсация")]
        public void VidjetView_Transaction_AccumulatedCompensation_FizikLimit()
        {
            _accumulatedCompensationSum = 700;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 1000;
            _transactionSum = 1500;
            _isCompensationAccumulated = false;
            _userName = "vidj_test39";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0) ;
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(200));
        }

        [Test, Description("Виджет в отпуске с заблокированным личным лимитом + накопленная компенсация")]
        public void VidjetView_Transaction_AccumulatedCompensation_FizikLimit_Week()
        {
            _accumulatedCompensationSum = 700;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 1000;
            _transactionSum = 700;
            _isCompensationAccumulated = true;
            _userName = "vidj_test40";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0) ;
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }
            */
            helper.DB.ExtendVacation();
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            var authenticator = new APIAuthenticator(_userName); // implements IAuthenticator
            var options = new RestClientOptions(_apiBaseV3)
            {
                Authenticator = authenticator
            };
            var client = new RestClient(options);
            var args = new
            {
                cateringPointId = 2,
                type = 10,
                sum = _transactionSum
            };
            var transaction = new RestRequest("/transactions/pay", Method.Post);
            transaction.AddJsonBody(args);
            RestResponse response = client.Execute(transaction);
            var content = response.Content;
            var dataApprove = JsonConvert.DeserializeObject<JsonRoot>(content);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VacationFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }
        #endregion

        #region VidgetNoCardAllowed

        [Test, Description("Запрет на привязку карты с личным лимитом и компенсацией")]
        public void VidjetNoCardAllowed_FizikLimit_Compensation()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 1000;
            _maxBalanceSum = 3000;
            _fizikLimitSum = 2000;
            _isCompensationAccumulated = false;
            _userName = "nocard_test1";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            if (helper.DB.IsCardBinded(_userName) == 0)
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_fizikLimitSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Запрет на привязку карты с заблокированным личным лимитом и компенсацией")]
        public void VidjetNoCardAllowed_FizikBlocked_Compensation()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 1000;
            _maxBalanceSum = 3000;
            _fizikLimitSum = 2000;
            _isCompensationAccumulated = false;
            _userName = "nocard_test2";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            if (helper.DB.IsCardBinded(_userName) == 0)
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VidjetHeader(), Is.EqualTo("Общая сумма"));
            Assert.That(helper.Dashboard.VidjetHeaderSum(), Is.EqualTo(_compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterFirstLine(), Is.EqualTo("Сумма компенсации"));
            Assert.That(helper.Dashboard.VidjetFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum + _compensationSum));
            Assert.That(helper.Dashboard.VidjetFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VidjetFooterSecondSum(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VidjetFooterFizikBlocked2(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }

        [Test, Description("Виджет с запретом карт и личным лимитом в отпуске")]
        public void VidjetNoCardAllowed_FizikLimit_Vacation()
        {
            _accumulatedCompensationSum = 0;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 200;
            _isCompensationAccumulated = false;
            _userName = "nocard_test3";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0)
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationNoAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_fizikLimitSum));
        }

        [Test, Description("Виджет с запретом привязки карты, накопленной компенсацией и заблокированным личным лимитом в отпуске")]
        public void VidjetNoCardAllowed_FizikBlocked_AccumulatedCompensation_Vacation()
        {
            _accumulatedCompensationSum = 400;
            _compensationSum = 0;
            _maxBalanceSum = 0;
            _fizikLimitSum = 200;
            _isCompensationAccumulated = false;
            _userName = "nocard_test4";
            Helper helper = new Helper(driver);
            helper.DB.SeenOnboarding(_userName);
            /*if (helper.DB.IsCardBinded(_userName) == 0)
            {
                helper.DB.InsertCard(_userName);
                helper.DB.UpdateBindCard(_userName);
            }*/
            helper.DB.SetBalance(_accumulatedCompensationSum, _compensationSum, _maxBalanceSum, _fizikLimitSum, _isCompensationAccumulated, _userName);
            helper.DB.ChargeOnWeekend(_userName, true);
            helper.DB.CompensationOnWeekend(_userName, true);
            helper.DB.UpdateIsFizikBlocked(1, _userName);
            helper.Navigation.NavigateToLoginPage();
            helper.Auth.Authorize(_userName);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(120));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]")));
            Assert.That(helper.Dashboard.VacationText(), Is.EqualTo(_vacationAccComp));
            Assert.That(helper.Dashboard.VacationFooterFirstLine(), Is.EqualTo("Накопленная компенсация"));
            Assert.That(helper.Dashboard.VacationFooterFirstSum(), Is.EqualTo(_accumulatedCompensationSum));
            Assert.That(helper.Dashboard.VacationFooterSecondLine(), Is.EqualTo("Личные средства"));
            Assert.That(helper.Dashboard.VacationLimitSumBlocked(), Is.EqualTo(0));
            Assert.That(helper.Dashboard.VacationFooterFizikBlocked(), Is.EqualTo("Недоступны в связи с наличием задолженности"));
        }
        #endregion
    }
}
