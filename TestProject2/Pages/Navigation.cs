﻿using OpenQA.Selenium;
using System;

namespace VidjetSpace
{
    internal class Navigation:HelperBase
    {
        internal Navigation(IWebDriver driver)
        {
            this.driver = driver;
        }
        internal void NavigateToMainPage()
        {
            driver.Navigate().GoToUrl("https://test.nalunch.me/customer/main");
        }

        internal void NavigateToLoginPage()
        {
            try
            {
                driver.Navigate().GoToUrl("https://test.nalunch.me/Account/Login/");
            }
            catch (WebDriverException ex) 
            {
                driver.Navigate().GoToUrl("https://test.nalunch.me/Account/Login/");
                Console.WriteLine(ex);
            }
            
        }
    }
}
