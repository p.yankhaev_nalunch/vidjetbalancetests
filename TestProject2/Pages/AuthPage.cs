﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace VidjetSpace
{
    internal class AuthPage:HelperBase
    {
        internal AuthPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void Authorize(string userName)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
            IWebElement element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id=\"UserName\"]")));
            Type(By.XPath("//*[@id=\"UserName\"]"), userName);
            Type(By.XPath("//*[@id=\"Password\"]"), "1234");
            driver.FindElement(By.XPath("/html/body/div[2]/section/div/div/div/form/div[3]/input")).Click();
        }
    }
}
