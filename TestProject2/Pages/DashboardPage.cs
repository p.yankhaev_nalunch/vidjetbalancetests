﻿using OpenQA.Selenium;

namespace VidjetSpace
{
    internal class DashboardPage:HelperBase
    {
        internal DashboardPage(IWebDriver driver) 
        {
            this.driver = driver;
        }
       
        public bool IsVidjetAvailable()
        {
            return IsElementPresent(By.CssSelector("#root > div > main > div:nth-child(2) > div > div > section.sc-fBWQRz.sc-uVWWZ.hmMLdW.jGcugM"));

        }

        #region standard_view
        public string VidjetHeader()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[1]/div/div[1]/div[1]")).Text;
            return c;
        }



        public int VidjetHeaderSum()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[1]/div/div[1]/div[2]")).Text;
            c = c.Substring(0, c.Length - 1);
            c = c.Replace(" ", "");
            int i = int.Parse(c);
            return i;
        }

        public string WholeSumWithOverdraft()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[1]/div/div[1]/div[3]")).Text;
            return c;
        }

        public string VidjetFooterFirstLine()
        {
            var c = driver.FindElement(By.XPath("/html/body/div[1]/div/main/div[2]/div/div/section[1]/div[2]/div[1]/div/div[1]")).Text;
            return c;
        }

        public int VidjetFooterFirstSum()
        {
            var c = driver.FindElement(By.XPath("/html/body/div[1]/div/main/div[2]/div/div/section[1]/div[2]/div[1]/div/div[2]")).Text;
            c = c.Substring(0, c.Length - 1);
            c = c.Replace(" ", "");
            int i = int.Parse(c);
            return i;
        }

        public string VidjetFooterSecondLine()
        {
            var c = driver.FindElement(By.XPath("/html/body/div[1]/div/main/div[2]/div/div/section[1]/div[2]/div[2]/div/div[1]")).Text;
            return c;
        }

        public int VidjetFooterSecondSum()
        {
            var l = driver.FindElement(By.XPath("/html/body/div[1]/div/main/div[2]/div/div/section[1]/div[2]/div[2]/div/div[2]")).Text;
            l = l.Substring(0, l.Length - 1);
            l = l.Replace(" ", "");
            int i = int.Parse(l);
            return i;
        }

        public string VidjetFooterFizikBlocked()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]")).Text;
            return c;
        }

        public string VidjetFooterFizikBlocked2()
        {
            var c = driver.FindElement(By.XPath("/html/body/div[1]/div/main/div[2]/div/div/section[1]/div[2]/div[2]/div[2]")).Text;
            return c;
        }
        #endregion

        #region vacation_view

        public string VacationText()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[1]/div[2]")).Text;
            return c;
        }

        public string VacationFooterFirstLine()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div[1]/div/div[1]")).Text;
            return c;
        }

        public string VacationFooterSecondLine()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div[2]/div[1]/div[1]")).Text;
            return c;
        }

        public int VacationFooterSecondSum()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div[2]/div/div[2]")).Text;
            c = c.Substring(0, c.Length - 1);
            c = c.Replace(" ", "");
            int i = int.Parse(c);
            return i;
        }
        public int VacationFooterFirstSum()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div[1]/div/div[2]")).Text;
            c = c.Substring(0, c.Length - 1);
            c = c.Replace(" ", "");
            int i = int.Parse(c);
            return i;
        }

        public int VacationLimitSumBlocked()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div/div[1]/div[2]/div")).Text;
            c = c.Substring(0, c.Length - 1);
            c = c.Replace(" ", "");
            int i = int.Parse(c);
            return i;
        }

        public string VacationFooterFizikBlocked()
        {
            var c = driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div[2]/div/div/section[1]/div[2]/div/div[2]")).Text;
            return c;
        }
        #endregion
    }
}
